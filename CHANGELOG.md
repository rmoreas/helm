## [4.1.5](https://gitlab.com/to-be-continuous/helm/compare/4.1.4...4.1.5) (2023-02-13)


### Bug Fixes

* **publish:** force publish chart built upstream ([63f482f](https://gitlab.com/to-be-continuous/helm/commit/63f482fdf5cee1eb9d1ca1a1be9ec492ba8e2176))

## [4.1.4](https://gitlab.com/to-be-continuous/helm/compare/4.1.3...4.1.4) (2023-02-01)


### Bug Fixes

* correct the output of helm-publish ([7ddb31e](https://gitlab.com/to-be-continuous/helm/commit/7ddb31e5bd8adeb820eb980c0711ae352cf6bb64)), closes [#35](https://gitlab.com/to-be-continuous/helm/issues/35)

## [4.1.3](https://gitlab.com/to-be-continuous/helm/compare/4.1.2...4.1.3) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([bf44e5e](https://gitlab.com/to-be-continuous/helm/commit/bf44e5e2b54fd1a4ec4128686ab1bf543dde335b))

## [4.1.2](https://gitlab.com/to-be-continuous/helm/compare/4.1.1...4.1.2) (2023-01-23)


### Bug Fixes

* **authent:** support Text or file variable for Kubeconfig ([67027d1](https://gitlab.com/to-be-continuous/helm/commit/67027d1483e93096639f2e8de247f494f4fd34c1))

## [4.1.1](https://gitlab.com/to-be-continuous/helm/compare/4.1.0...4.1.1) (2023-01-17)


### Bug Fixes

* HELM_SEMREL_RELEASE_DISABLED variable not taken into account ([9529cb7](https://gitlab.com/to-be-continuous/helm/commit/9529cb74509605c0abdca1567a0e3430b82af880))

# [4.1.0](https://gitlab.com/to-be-continuous/helm/compare/4.0.1...4.1.0) (2023-01-17)


### Bug Fixes

* use new kube-score image with explicit registry (Docker Hub) ([af8b73c](https://gitlab.com/to-be-continuous/helm/commit/af8b73c5a3114f3b5f13ec9bcb9eff7f657094a7))


### Features

* **package:** propagate output variables (dotenv) ([09af4af](https://gitlab.com/to-be-continuous/helm/commit/09af4af537a7c4411de0efcf7bc7b31de3cbf67e))
* **sast:** run kube-score & values lint against cascading environments ([71386e3](https://gitlab.com/to-be-continuous/helm/commit/71386e3798a621de66bbdd6eccf70d5fa56caa6c))

## [4.0.1](https://gitlab.com/to-be-continuous/helm/compare/4.0.0...4.0.1) (2023-01-13)


### Bug Fixes

* **deploy:** deploy packaged chart instead of chart file (required for chart with dependencies) ([32356cc](https://gitlab.com/to-be-continuous/helm/commit/32356cc3877a6b8d3e7a46aab8610b4fb3ea41db))

# [4.0.0](https://gitlab.com/to-be-continuous/helm/compare/3.3.2...4.0.0) (2023-01-11)


### Bug Fixes

* check chart is present locally on publish job ([8cabd71](https://gitlab.com/to-be-continuous/helm/commit/8cabd719a222f688657357474f548af9fcb51a92))


### Code Refactoring

* remove old variables and values ([928c644](https://gitlab.com/to-be-continuous/helm/commit/928c64433e5d32dab341d9217f86497871e26237))


### Features

* environments activation is now opt-in ([64a0a2f](https://gitlab.com/to-be-continuous/helm/commit/64a0a2f703432485a9269138004db0ee433bdea4))
* **package:** support publish snapshot (untested) version ([ed861d9](https://gitlab.com/to-be-continuous/helm/commit/ed861d9e2fcd0462037b2db333aec9e0aac7f678))
* semrel integration no longer increases the app version ([698111e](https://gitlab.com/to-be-continuous/helm/commit/698111e61f52a3f21fa9699c26e005a601af7734))
* support OCI-based registry as pull repos & support user/password authentication ([7ea406a](https://gitlab.com/to-be-continuous/helm/commit/7ea406aad0e2c7817950f08a74be3ecb52afd8ca))
* support various publish methods (push, post, put, custom) with auto detection ([676c41c](https://gitlab.com/to-be-continuous/helm/commit/676c41cb30a40899324a1610cc7bb915ffc38b5a))


### BREAKING CHANGES

* HELM_xxx_DISABLED variables no longer supported to enable/disable environments (see doc)
* default publish URL uses GitLab registry
* former dynamic variables $env and $appname removed: use $environment_type and $environment_name instead
* former dynamic value {{env}} removed: use {{environment_type}} instead

## [3.3.2](https://gitlab.com/to-be-continuous/helm/compare/3.3.1...3.3.2) (2022-12-18)


### Bug Fixes

* rename Helm value environment_type to environmentType (complies to naming conventions) ([2d8c922](https://gitlab.com/to-be-continuous/helm/commit/2d8c92224f38feb4253b508e73cdb40077cd657c))

## [3.3.1](https://gitlab.com/to-be-continuous/helm/compare/3.3.0...3.3.1) (2022-12-17)


### Bug Fixes

* hanging awk script ([7294a75](https://gitlab.com/to-be-continuous/helm/commit/7294a75a45147a7842290bdb666e09b765cd2712))

# [3.3.0](https://gitlab.com/to-be-continuous/helm/compare/3.2.0...3.3.0) (2022-12-17)


### Features

* support dynamic env url ([6e6dd77](https://gitlab.com/to-be-continuous/helm/commit/6e6dd777511c86b0967c398e221e1b1ae30c43e2))

# [3.2.0](https://gitlab.com/to-be-continuous/helm/compare/3.1.0...3.2.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([23357c5](https://gitlab.com/to-be-continuous/helm/commit/23357c584d18f2480cae9cdf34543f6e6319a417))

# [3.1.0](https://gitlab.com/to-be-continuous/helm/compare/3.0.1...3.1.0) (2022-11-26)


### Features

* switch score job to package-test stage ([5b60640](https://gitlab.com/to-be-continuous/helm/commit/5b60640d0b599888c80d352345d185b878902740))

## [3.0.1](https://gitlab.com/to-be-continuous/helm/compare/3.0.0...3.0.1) (2022-09-09)


### Bug Fixes

* use chart var if no Chart.yml in directory ([12cbad9](https://gitlab.com/to-be-continuous/helm/commit/12cbad976e45066b1448ac687cf133f764510f54))

# [3.0.0](https://gitlab.com/to-be-continuous/helm/compare/2.4.1...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([f13ad25](https://gitlab.com/to-be-continuous/helm/commit/f13ad2591065d2f815ff772d4ccd6acaeae96a25))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.4.1](https://gitlab.com/to-be-continuous/helm/compare/2.4.0...2.4.1) (2022-06-30)


### Bug Fixes

* enforce AUTODEPLOY_TO_PROD as boolean variable ([5e9a58f](https://gitlab.com/to-be-continuous/helm/commit/5e9a58faef684443e4199f6f06905f05d576b696))

# [2.4.0](https://gitlab.com/to-be-continuous/helm/compare/2.3.0...2.4.0) (2022-06-30)


### Features

* enforce AUTODEPLOY_TO_PROD and PUBLISH_ON_PROD as boolean variables ([87ff9a6](https://gitlab.com/to-be-continuous/helm/commit/87ff9a62e4f7fc730c42a8a24dfc5af9c10c135f))

# [2.3.0](https://gitlab.com/to-be-continuous/helm/compare/2.2.0...2.3.0) (2022-05-01)


### Features

* configurable tracking image ([27d3406](https://gitlab.com/to-be-continuous/helm/commit/27d3406688e96edd7fd35070e559df29b565fbf3))

# [2.2.0](https://gitlab.com/to-be-continuous/helm/compare/2.1.0...2.2.0) (2022-03-29)


### Features

* add support for a common value file ([53c0210](https://gitlab.com/to-be-continuous/helm/commit/53c0210419fbd9fe8a4ba20a1817c49ab82d5b97))

# [2.1.0](https://gitlab.com/to-be-continuous/helm/compare/2.0.6...2.1.0) (2022-03-17)


### Bug Fixes

* **kicker:** syntax ([076fd01](https://gitlab.com/to-be-continuous/helm/commit/076fd011be11c1cc9663357c4a3bca110308439d))


### Features

* add helm publish to a repository ([65c7924](https://gitlab.com/to-be-continuous/helm/commit/65c792475fab55d8eedeb65bd2fe0fd61ae2eb98))

## [2.0.6](https://gitlab.com/to-be-continuous/helm/compare/2.0.5...2.0.6) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([6929973](https://gitlab.com/to-be-continuous/helm/commit/69299738c8a8b47a8a7dde3e45561285f9a84f6f))

## [2.0.5](https://gitlab.com/to-be-continuous/helm/compare/2.0.4...2.0.5) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([6753e54](https://gitlab.com/to-be-continuous/helm/commit/6753e54f5f1b6d7fd204492c20fe571439a09c11))

## [2.0.4](https://gitlab.com/to-be-continuous/helm/compare/2.0.3...2.0.4) (2022-01-10)


### Bug Fixes

* non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([103f6f3](https://gitlab.com/to-be-continuous/helm/commit/103f6f32b6edab1f127b5c8f062f25a98e00f081))

## [2.0.3](https://gitlab.com/to-be-continuous/helm/compare/2.0.2...2.0.3) (2021-11-15)


### Bug Fixes

* **kube-score:** dependency update before kube score ([7514d69](https://gitlab.com/to-be-continuous/helm/commit/7514d6946aa7196f7cd3b67d4e79c981cd4a4fd5))

## [2.0.2](https://gitlab.com/to-be-continuous/helm/compare/2.0.1...2.0.2) (2021-11-09)


### Bug Fixes

* score and lint jobs are each launched on their corresponding branch ([71588a3](https://gitlab.com/to-be-continuous/helm/commit/71588a384ca086e70f570a6b7c5ad29bea292e9b))

## [2.0.1](https://gitlab.com/to-be-continuous/helm/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([45d46af](https://gitlab.com/to-be-continuous/helm/commit/45d46af8c2c38355aaffc68e98473cd73121d33e))

## [2.0.0](https://gitlab.com/to-be-continuous/helm/compare/1.4.2...2.0.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([7c72363](https://gitlab.com/to-be-continuous/helm/commit/7c72363c466beb98624d64c220bfa7c1445d4586))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.4.2](https://gitlab.com/to-be-continuous/helm/compare/1.4.1...1.4.2) (2021-07-26)

### Bug Fixes

* add an option to avoid var conflict name ([b6e98a7](https://gitlab.com/to-be-continuous/helm/commit/b6e98a7021e964457a0397ff86e20fe4cb12751d))

## [1.4.1](https://gitlab.com/to-be-continuous/helm/compare/1.4.0...1.4.1) (2021-07-07)

### Bug Fixes

* conflict between vault and scoped vars ([f3e89c0](https://gitlab.com/to-be-continuous/helm/commit/f3e89c0070760459487c93a4e88f608974a6e920))

## [1.4.0](https://gitlab.com/to-be-continuous/helm/compare/1.3.0...1.4.0) (2021-06-19)

### Features

* support multi-lines environment variables substitution ([818ca76](https://gitlab.com/to-be-continuous/helm/commit/818ca767484bb6d623102c5905449ddabf0b7335))

## [1.3.0](https://gitlab.com/to-be-continuous/helm/compare/1.2.1...1.3.0) (2021-06-10)

### Features

* move group ([9306f3c](https://gitlab.com/to-be-continuous/helm/commit/9306f3c06f703220935e7737c3f91686087efa26))

## [1.2.1](https://gitlab.com/Orange-OpenSource/tbc/helm/compare/1.2.0...1.2.1) (2021-06-09)

### Bug Fixes

* **publish:** remove variable from rules:exists expression (unsupported) ([47a1246](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/47a1246d811b2ad484f19db95656183ce1de94c7))

## [1.2.0](https://gitlab.com/Orange-OpenSource/tbc/helm/compare/1.1.1...1.2.0) (2021-05-18)

### Features

* add scoped variables support ([abdc875](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/abdc875fe5d820233a16db86a1c35a3d989e9405))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/helm/compare/1.1.0...1.1.1) (2021-05-17)

### Bug Fixes

* wrong cache scope ([033a82e](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/033a82eea5a4a1b846aaee5561a417a3085663e9))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/helm/compare/1.0.0...1.1.0) (2021-05-12)

### Features

* integrate semantic-release info to helm-package job ([d3aaf7c](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/d3aaf7c19212400ac325ee8888af88e061e2e9b9))

## 1.0.0 (2021-05-06)

### Features

* initial release ([293fbec](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/293fbecbd1b9f33bdb321ed1c84ee3a0bdb94648))
